acidobacteria
aconoidasida
actinobacteria
actinobacteria
actinopterygii
agaricales
agaricomycetes
alphabaculovirus
alphaherpesvirinae
alphaproteobacteria
alteromonadales
alveolata
apicomplexa
aquificae
arachnida
archaea
arthropoda
ascomycota
aves
aviadenovirus
bacillales
bacilli
bacteria
bacteroidales
bacteroidetes-chlorobi
bacteroidetes
bacteroidia
baculoviridae
basidiomycota
bclasvirinae
betabaculovirus
betaherpesvirinae
betaproteobacteria
boletales
brassicales
burkholderiales
campylobacterales
capnodiales
carnivora
cellvibrionales
cetartiodactyla
chaetothyriales
cheoctovirus
chlamydiae
chlorobi
chloroflexi
chlorophyta
chordopoxvirinae
chromatiales
chroococcales
clostridia
clostridiales
coccidia
coriobacteriales
coriobacteriia
corynebacteriales
cyanobacteria
cyprinodontiformes
cytophagales
cytophagia
delta-epsilon-subdivisions
deltaproteobacteria
desulfobacterales
desulfovibrionales
desulfurococcales
desulfuromonadales
diptera
dothideomycetes
embryophyta
endopterygota
enquatrovirus
enterobacterales
entomoplasmatales
epsilonproteobacteria
euarchontoglires
eudicots
euglenozoa
eukaryota
eurotiales
eurotiomycetes
euryarchaeota
eutheria
fabales
firmicutes
flavobacteriales
flavobacteriia
fromanvirus
fungi
fusobacteria
fusobacteriales
gammaherpesvirinae
gammaproteobacteria
glires
glomerellales
guernseyvirinae
halobacteria
halobacteriales
haloferacales
helotiales
hemiptera
herpesviridae
hymenoptera
hypocreales
insecta
iridoviridae
lactobacillales
laurasiatheria
legionellales
leotiomycetes
lepidoptera
liliopsida
mammalia
metazoa
methanobacteria
methanococcales
methanomicrobia
methanomicrobiales
micrococcales
microsporidia
mollicutes
mollusca
mucorales
mucoromycota
mycoplasmatales
natrialbales
neisseriales
nematoda
nitrosomonadales
nostocales
oceanospirillales
onygenales
oscillatoriales
pahexavirus
passeriformes
pasteurellales
peduovirus
planctomycetes
plasmodium
pleosporales
poales
polyporales
poxviridae
primates
propionibacteriales
proteobacteria
pseudomonadales
rhizobiales
rhizobium-agrobacterium
rhodobacterales
rhodospirillales
rickettsiales
rudiviridae
saccharomycetes
sauropsida
selenomonadales
simplexvirus
skunavirus
solanales
sordariomycetes
sphingobacteriia
sphingomonadales
spirochaetales
spirochaetes
spirochaetia
spounavirinae
stramenopiles
streptomycetales
streptosporangiales
sulfolobales
synechococcales
synergistetes
tenericutes
tequatrovirus
teseptimavirus
tetrapoda
tevenvirinae
thaumarchaeota
thermoanaerobacterales
thermoplasmata
thermoproteales
thermoprotei
thermotogae
thiotrichales
tissierellales
tissierellia
tremellomycetes
tunavirinae
varicellovirus
verrucomicrobia
vertebrata
vibrionales
viridiplantae
xanthomonadales

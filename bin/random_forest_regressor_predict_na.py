#!/usr/bin/env python

###########################################################
#                                                         #
# random_forest_regressor_predict_na.py - F1 prediction   #
# with random forest regressor and feature matrix if      #
# free energy and gc ratio are N/A                        #
#                                                         #
# Developed by Cynthia Webster (2022)                     #
# Plant Computational Genomics Lab                        #
# University of Connecticut                               #
#                                                         #
# This script is under the MIT License                    #
#                                                         #
###########################################################

from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import pandas as pd
import numpy as np
from sklearn.metrics import f1_score,mean_squared_error, accuracy_score
from sklearn.metrics import r2_score,mean_squared_error
from sklearn.feature_selection import RFECV
import matplotlib.pyplot as plt
import sys
import re

def keep_na(df):
    assert isinstance(df, pd.DataFrame), "df needs to be a pd.DataFrame"
    df.dropna(inplace=False)
    indices_to_keep = df.isin([np.nan, np.inf, -np.inf]).any(1)
    return df[indices_to_keep]

def clean_dataset(df):
    assert isinstance(df, pd.DataFrame), "df needs to be a pd.DataFrame"
    df.dropna(inplace=True)
    indices_to_keep = ~df.isin([np.nan, np.inf, -np.inf]).any(1)
    return df[indices_to_keep]

training_set = sys.argv[1]
testing_set = sys.argv[2]
output = sys.argv[3]

raw_data = pd.read_csv(training_set, delimiter="\t", low_memory=False)
data = raw_data.drop('Free_Energy', axis=1)
data = data.drop('GC_Ratio', axis=1)
data = clean_dataset(data)

#create the labels, or field we are trying to estimate
label = data['F1']

# drop Target and F1
data = data.drop('Target', axis=1)
data = data.drop('F1', axis=1)

rf_all = RandomForestRegressor(n_estimators = 100)
rf_all.fit(data, label)

new_raw_data = pd.read_csv(testing_set, delimiter=r"\s+", low_memory=False)
new_data = keep_na(new_raw_data)
new_data = new_data.drop('Free_Energy', axis=1)
new_data = new_data.drop('GC_Ratio', axis=1)
new_order_numbers = new_data['Transcript']

new_data = new_data.drop(['Gene','Transcript'], axis = 1)
print(list(new_data.columns))
rf_pred = rf_all.predict(new_data)

res = pd.DataFrame({'Transcript': new_order_numbers, 'Prediction': rf_pred})
res.to_csv(output, index=False, header=False)

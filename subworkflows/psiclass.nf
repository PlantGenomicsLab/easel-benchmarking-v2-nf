include { combineAll_psiclass; fix_exons_psiclass } from '../modules/psiclass_prediction.nf'

workflow assembly_psiclass {
    take:
    est
    protein

    main:
    combineAll_psiclass ( est, protein )
	fix_exons_psiclass ( combineAll_psiclass.out.psiclass_combined )

	emit:
	fix_exons_psiclass.out
	
}

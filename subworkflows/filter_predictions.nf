include { psiclass_utr_trim; stringtie2_utr_trim; psiclass_filter; stringtie2_filter; merge_gff; gene_overlap; format_gff; list } from '../modules/combine_predictions.nf'
include { gtf } from '../modules/filtering/tasks_gff_to_gtf.nf'
include { featureCounts; gene_expression } from '../modules/filtering/tasks_gene_expression.nf'
include { orthodb_bed; orthodb_feature } from '../modules/filtering/tasks_orthodb.nf'
include { support } from '../modules/filtering/tasks_support.nf'
include { mono_multi } from '../modules/filtering/tasks_multi_mono.nf' 
include { unfiltered_protein; unfiltered_nucleotide; eggnog; eggnog_feature } from '../modules/filtering/tasks_gene_family.nf' 
include { start_stop_codons } from '../modules/filtering/tasks_full_length.nf' 
include { cds_extract; cds_repeat_content } from '../modules/filtering/tasks_repeat_cds.nf' 
include { map } from '../modules/filtering/tasks_map.nf' 
include { transcript_extract; length; molecular_weight; merge_transcript; gc; gc_python } from '../modules/filtering/tasks_transcript.nf'
include { true_start_site; pad; split_fasta; rna_fold; merge; matrix } from '../modules/filtering/tasks_folding.nf' 
include { regressor; classifier; filtered_gtf; filtered_gff; filtered_protein; filtered_nucleotide } from '../modules/filtering/tasks_random_forest.nf'

workflow filter {

    take:
    stringtie2_combined
    psiclass_combined
    genome
    s_est
    s_prot
    p_est
    p_prot
    eggnog_db
    orthodb_align
    stringtie2_trans
    psiclass_trans


    main:
    psiclass_utr_trim       ( psiclass_trans )
    stringtie2_utr_trim     ( stringtie2_trans )
    psiclass_filter         ( psiclass_utr_trim.out.psi_utr, genome )
    stringtie2_filter       ( stringtie2_utr_trim.out.str_utr, genome )
    psiclass_filter         ( psiclass_trans, genome )
    stringtie2_filter       ( stringtie2_trans, genome )
    merge_gff		        ( stringtie2_combined.collect(), psiclass_combined.collect(), stringtie2_filter.out.str_start, psiclass_filter.out.psi_start )
    gene_overlap	    ( merge_gff.out.merged_unfiltered )
    format_gff              ( gene_overlap.out.overlap_unfiltered, genome, params.prefix)
    list                    ( s_est, s_prot, p_est, p_prot, stringtie2_filter.out.str_start, psiclass_filter.out.psi_start )       
    gtf                     ( format_gff.out.unfiltered_fixed, params.prefix )
    bam = Channel.fromPath(params.bam).flatten().map { file -> tuple(file.baseName, file) }
    featureCounts           ( bam, gtf.out.unfiltered_gtf )
    orthodb_bed             ( orthodb_align, format_gff.out.unfiltered_fixed )
    support                 ( format_gff.out.unfiltered_fixed, list.out.gff )
    orthodb_feature         ( support.out.support, orthodb_bed.out.bed )
    gene_expression         ( featureCounts.out.counts.collect(), support.out.support)
    mono_multi              ( format_gff.out.unfiltered_fixed, support.out.support )
    unfiltered_protein      ( genome, gtf.out.unfiltered_gtf, params.prefix )
    unfiltered_nucleotide   ( genome, gtf.out.unfiltered_gtf, params.prefix )
    eggnog                  ( unfiltered_protein.out.protein, eggnog_db )
    eggnog_feature          ( support.out.support, eggnog.out.mapper )
    start_stop_codons       ( format_gff.out.unfiltered_fixed, support.out.support )
    cds_extract             ( genome, gtf.out.unfiltered_gtf, support.out.support )
    cds_repeat_content      ( cds_extract.out.cds, support.out.support )
    transcript_extract      ( genome, gtf.out.unfiltered_gtf, support.out.support, params.parts )
    transcript_ch = transcript_extract.out.transcript_part.flatten().map { file -> tuple(file.baseName, file) }
    length                  ( transcript_ch )
    molecular_weight        ( transcript_ch )
    merge_transcript        ( molecular_weight.out.weight_txt.collect(), length.out.length_txt.collect(), support.out.support)
    gc                      ( transcript_extract.out.transcript )
    gc_python               ( support.out.support, gc.out.gc )
    true_start_site         ( format_gff.out.unfiltered_fixed )
    pad                     ( params.window, true_start_site.out.text, genome )
    split_fasta             ( pad.out.fasta, params.parts )
    fasta_ch = split_fasta.out.split.flatten().map { file -> tuple(file.baseName, file) }
    rna_fold                ( fasta_ch )
    merge                   ( rna_fold.out.rna_fold.collect() )
	matrix                  ( params.window, support.out.support, merge.out.merge_rna )
    map                     ( matrix.out.gc_ratio.collect(), matrix.out.free_energy.collect(), gc_python.out.gc_track.collect(), merge_transcript.out.length.collect(), merge_transcript.out.weight.collect(), support.out.support.collect(), mono_multi.out.exonic.collect(), eggnog_feature.out.eggnog.collect(), start_stop_codons.out.start_codon.collect(), start_stop_codons.out.stop_codon.collect(), cds_repeat_content.out.cds_repeat.collect(), orthodb_feature.out.orthodb.collect(), gene_expression.out.expression.collect())

    if(params.training_set == "plant"){    
    regressor               ( map.out.features, params.plant )
    classifier              ( map.out.features, params.plant )
    }
    else if(params.training_set == "vertebrate"){    
    regressor               ( map.out.features, params.vertebrate )
    classifier              ( map.out.features, params.vertebrate )
    }
    else if(params.training_set == "invertebrate"){
    regressor               ( map.out.features, params.invertebrate )
    classifier              ( map.out.features, params.invertebrate )
    }	
    filtered_gtf            ( classifier.out.c_rf, regressor.out.r_rf, gtf.out.unfiltered_gtf, params.prefix, params.regressor )
    filtered_gff            ( filtered_gtf.out.filtered_prediction, params.prefix )
    filtered_protein        ( filtered_gtf.out.filtered_prediction, genome, params.prefix )
    filtered_nucleotide     ( genome, filtered_gtf.out.filtered_prediction, params.prefix )

    emit:
    format_gff.out
    unfiltered_protein.out.protein
    filtered_protein.out
    filtered_gff.out.filtered_gff
    filtered_gtf.out.filtered_prediction
    regressor.out
}

include { combineAll_stringtie2; fix_exons_stringtie2 } from '../modules/stringtie2_prediction.nf'

workflow assembly_stringtie2 {
    take:
    est
    protein

    main:
    combineAll_stringtie2 ( est, protein )
	fix_exons_stringtie2 (combineAll_stringtie2.out.stringtie2_combined)

    emit:
	fix_exons_stringtie2.out
}

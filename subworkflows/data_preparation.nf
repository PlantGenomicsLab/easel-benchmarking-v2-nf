include { fold_genome } from '../modules/split_genome.nf'
include { entap } from '../modules/databases.nf'

workflow data_input {

    take:
    genome
	
    main:
    entap               ( params.config_ini )
    fold_genome         ( genome, params.prefix )

    emit:
    entap.out.data_eggnog
    entap.out.entap_db
    entap.out.eggnog_db
    fold_genome.out
}

include { reference; mikado; gtf; gff; nucleotide; protein; agat_isoform; diamond_isoform; busco_isoform; filtered_primary_output } from '../modules/keep_primary_isoform.nf'

workflow primary_isoform {

    take:
    gtf
    regressor
    genome
    
    main:
    gtf         ( gtf, regressor, params.prefix )
    gff         ( gtf.out.filtered_gtf, params.prefix )
   
    if(params.reference !== null){
        reference       ( params.reference, params.prefix )
        mikado          ( reference.out.gtf, gtf.out.filtered_gtf, params.prefix )
    }

    nucleotide  ( genome, gff.out.filtered_gff, params.prefix )
    protein     ( gff.out.filtered_gff, genome, params.prefix )
    agat_isoform        ( gff.out.filtered_gff, params.prefix )
    diamond_isoform     ( protein.out.protein, params.reference_db, params.prefix, params.qcoverage, params.tcoverage )
    busco_isoform       ( protein.out.protein, params.busco_lineage, params.prefix )
    filtered_primary_output (busco_isoform.out.busco_filtered_txt, agat_isoform.out.filtered_stats, diamond_isoform.out.stats)
  
    emit:
    filtered_primary_output.out
}

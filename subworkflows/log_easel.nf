include { easel_output } from '../modules/log.nf'

workflow output {

    take:
    unfiltered
    filtered
    filter_primary
    filter_longest
	
    main:
    easel_output    ( unfiltered, filtered, filter_primary?:'', filter_longest?:'')
	
}

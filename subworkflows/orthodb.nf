include { fix_exons_stringtie2; fix_exons_psiclass } from '../modules/extrinsic_evidence.nf'

workflow orthodb_hints {
   
	take:
	psiclass
	stringtie2
	
    main:
	fix_exons_psiclass   		    ( psiclass )
	fix_exons_stringtie2			( stringtie2 )
	
	emit:
	fix_exons_psiclass.out
	fix_exons_stringtie2.out

}

include { reference; mikado; gtf; gff; nucleotide; busco; agat; no_contam; final_log } from '../modules/contam_removal.nf'

workflow remove_contam {

    take:
    gtf
    protein
    ss_protein
    genome
    odb
    unfiltered
    filtered
    filter_primary
    filter_longest

    
    main:
    gtf         ( protein, gtf, params.prefix )
    gff         ( gtf.out.no_contam, params.prefix )

    if(params.reference !== null){
        reference       ( params.reference, params.prefix )
        mikado          ( reference.out.gtf, gtf.out.no_contam, params.prefix )
    } 

    nucleotide  ( genome, gff.out.filtered_gff, params.prefix )
    busco       ( protein, odb )
    agat        ( protein, gff.out.filtered_gff, params.prefix )
    no_contam   ( busco.out.contam_txt, agat.out.contam_stats, ss_protein, params.contam )
    final_log   ( no_contam.out.contam_log, genome_log, rna_reads, alignments, stringtie2, psiclass, unfiltered, filtered, filter_primary?:'', filter_longest?:'')
}

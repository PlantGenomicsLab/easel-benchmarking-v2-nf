/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE INPUTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

def valid_params = [
    training       : ['plant', 'invertebrate', 'vertebrate'],
    orthodb        : ['Actinopterygii', 'Cyprinodontiformes', 'Cichliformes', 'Alveolata', 'Apicomplexa', 'Ciliophora', 'Coccidia', 'Aconoidasida', 'Cryptosporidium', 'Plasmodium', 'Sarcocystidae', 'Eimeria', 'Piroplasmida', 'Plasmodium', 'Babesia', 'Plasmodium', 'Theileriidae', 'Plasmodium', 'Amoebozoa', 'Eumycetozoa', 'Arthropoda', 'Crustacea', 'Hexapoda', 'Arachnida', 'Insecta', 'Acari', 'Araneae', 'Hemiptera', 'Holometabola', 'Lepidoptera', 'Hymenoptera', 'Diptera', 'Coleoptera', 'Brachycera', 'Polyphaga', 'Papilionoidea', 'Aculeata', 'Nematocera', 'Formicidae', 'Apoidea', 'Glossina', 'Culicidae', 'Drosophilidae', 'Drosophila', 'Anopheles', 'Cnidaria', 'Anthozoa', 'Euglenozoa', 'Trypanosoma', 'Leishmaniinae', 'Leishmania', 'Lophotrochozoa', 'Mollusca', 'Mammalia', 'Eutheria', 'Metatheria', 'Laurasiatheria', 'Euarchontoglires', 'Glires', 'Eulipotyphla', 'Artiodactyla', 'Carnivora', 'Primates', 'Rodentia', 'Cetacea', 'Cercopithecoidea', 'Hominoidea', 'Hominidae', 'Nematoda', 'Chromadorea', 'Rhodophyta', 'Sauropsida', 'Squamata', 'Aves', 'Gruiformes', 'Passeriformes', 'Falconiformes', 'Pelecaniformes', 'Galloanserae', 'Stramenopiles', 'Oomycota', 'Bacillariophyta', 'Saprolegniaceae', 'Phytophthora', 'Viridiplantae', 'Embryophyta', 'Chlorophyta', 'eudicotyledons', 'Liliopsida', 'Chlorophyceae', 'Trebouxiophyceae', 'Mamiellales', 'Malvales', 'Brassicales', 'Poales', 'Fabales', 'Lamiales', 'Malpighiales', 'Solanales', 'Rosales', 'Rosaceae', 'Nicotiana','Tetrapoda'],
    busco          : ['acidobacteria', 'aconoidasida', 'actinobacteria', 'actinobacteria', 'actinopterygii', 'agaricales', 'agaricomycetes', 'alphabaculovirus', 'alphaherpesvirinae', 'alphaproteobacteria', 'alteromonadales', 'alveolata', 'apicomplexa', 'aquificae', 'arachnida', 'archaea', 'arthropoda', 'ascomycota', 'aves', 'aviadenovirus', 'bacillales', 'bacilli', 'bacteria', 'bacteroidales', 'bacteroidetes-chlorobi', 'bacteroidetes', 'bacteroidia', 'baculoviridae', 'basidiomycota', 'bclasvirinae', 'betabaculovirus', 'betaherpesvirinae', 'betaproteobacteria', 'boletales', 'brassicales', 'burkholderiales', 'campylobacterales', 'capnodiales', 'carnivora', 'cellvibrionales', 'cetartiodactyla', 'chaetothyriales', 'cheoctovirus', 'chlamydiae', 'chlorobi', 'chloroflexi', 'chlorophyta', 'chordopoxvirinae', 'chromatiales', 'chroococcales', 'clostridia', 'clostridiales', 'coccidia', 'coriobacteriales', 'coriobacteriia', 'corynebacteriales', 'cyanobacteria', 'cyprinodontiformes', 'cytophagales', 'cytophagia', 'delta-epsilon-subdivisions', 'deltaproteobacteria', 'desulfobacterales', 'desulfovibrionales', 'desulfurococcales', 'desulfuromonadales', 'diptera', 'dothideomycetes', 'embryophyta', 'endopterygota', 'enquatrovirus', 'enterobacterales', 'entomoplasmatales', 'epsilonproteobacteria', 'euarchontoglires', 'eudicots', 'euglenozoa', 'eukaryota', 'eurotiales', 'eurotiomycetes', 'euryarchaeota', 'eutheria', 'fabales', 'firmicutes', 'flavobacteriales', 'flavobacteriia', 'fromanvirus', 'fungi', 'fusobacteria', 'fusobacteriales', 'gammaherpesvirinae', 'gammaproteobacteria', 'glires', 'glomerellales', 'guernseyvirinae', 'halobacteria', 'halobacteriales', 'haloferacales', 'helotiales', 'hemiptera', 'herpesviridae', 'hymenoptera', 'hypocreales', 'insecta', 'iridoviridae', 'lactobacillales', 'laurasiatheria', 'legionellales', 'leotiomycetes', 'lepidoptera', 'liliopsida', 'mammalia', 'metazoa', 'methanobacteria', 'methanococcales', 'methanomicrobia', 'methanomicrobiales', 'micrococcales', 'microsporidia', 'mollicutes', 'mollusca', 'mucorales', 'mucoromycota', 'mycoplasmatales', 'natrialbales', 'neisseriales', 'nematoda', 'nitrosomonadales', 'nostocales', 'oceanospirillales', 'onygenales', 'oscillatoriales', 'pahexavirus', 'passeriformes', 'pasteurellales', 'peduovirus', 'planctomycetes', 'plasmodium', 'pleosporales', 'poales', 'polyporales', 'poxviridae', 'primates', 'propionibacteriales', 'proteobacteria', 'pseudomonadales', 'rhizobiales', 'rhizobium-agrobacterium', 'rhodobacterales', 'rhodospirillales', 'rickettsiales', 'rudiviridae', 'saccharomycetes', 'sauropsida', 'selenomonadales', 'simplexvirus', 'skunavirus', 'solanales', 'sordariomycetes', 'sphingobacteriia', 'sphingomonadales', 'spirochaetales', 'spirochaetes', 'spirochaetia', 'spounavirinae', 'stramenopiles', 'streptomycetales', 'streptosporangiales', 'sulfolobales', 'synechococcales', 'synergistetes', 'tenericutes', 'tequatrovirus', 'teseptimavirus', 'tetrapoda', 'tevenvirinae', 'thaumarchaeota', 'thermoanaerobacterales', 'thermoplasmata', 'thermoproteales', 'thermoprotei', 'thermotogae', 'thiotrichales', 'tissierellales', 'tissierellia', 'tremellomycetes', 'tunavirinae', 'varicellovirus', 'verrucomicrobia', 'vertebrata', 'vibrionales', 'viridiplantae', 'xanthomonadales']
]

def summary_params = NfcoreSchema.paramsSummaryMap(workflow, params)

// Validate input parameters
WorkflowEasel.initialise(params, log, valid_params)

// Check input path parameters to see if they exist
checkPathParamList = [
    params.genome, params.taxon, params.busco_lineage, params.order, params.outdir, params.prefix, params.external_protein, params.config_est, params.config_protein, params.training_set, params.bins, params.total_reads, params.mean_length, params.rate, params.hisat2_min_intronlen, params.hisat2_max_intronlen, params.gap_tolerance, params.cluster_id, params.test_count, params.train_count, params.kfold, params.rounds, params.optimize, params.tcoverage, params.qcoverage, params.reference_db, params.config_ini, params.window, params.parts, params.regressor
]

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT LOCAL MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { data_input } from '../subworkflows/data_preparation.nf'
include { assembly_psiclass } from '../subworkflows/psiclass.nf'
include { assembly_stringtie2 } from '../subworkflows/stringtie2.nf'
include { orthodb_hints } from '../subworkflows/orthodb.nf'
include { filter } from '../subworkflows/filter_predictions.nf'
include { summary_stats } from '../subworkflows/quality_metrics.nf'
include { output } from '../subworkflows/log_easel.nf'
include { extrinsic_filter } from '../subworkflows/filter_extrinsic_evidence.nf'
include { remove_contam } from '../subworkflows/contam.nf'
include { primary_isoform } from '../subworkflows/primary.nf'
include { longest_isoform } from '../subworkflows/isoform.nf'

workflow main_workflow {
	
	data_input 				( params.genome ) 
    assembly_stringtie2     ( data_input.out[0], data_input.out[10], data_input.out[2], params.config_est, params.config_protein, data_input.out[6], data_input.out[11] )
	assembly_psiclass       ( data_input.out[1], data_input.out[10], data_input.out[2], params.config_est, params.config_protein, data_input.out[6], data_input.out[11] )

	if(params.external_protein == true){
		orthodb_hints			( data_input.out[10], data_input.out[2], params.config_protein, assembly_psiclass.out[5], assembly_stringtie2.out[5], data_input.out[9])
		extrinsic_filter	 	( assembly_stringtie2.out[0], assembly_psiclass.out[0], data_input.out[10], assembly_stringtie2.out[1], assembly_stringtie2.out[2], assembly_psiclass.out[1], assembly_psiclass.out[2], orthodb_hints.out[0], orthodb_hints.out[1], data_input.out[6], data_input.out[0], data_input.out[9], assembly_stringtie2.out[3], assembly_psiclass.out[3] )
		summary_stats		    ( extrinsic_filter.out[1], params.busco_lineage, extrinsic_filter.out[0], extrinsic_filter.out[3], extrinsic_filter.out[2], data_input.out[6], data_input.out[7], data_input.out[8] )
		if(params.primary_isoform == true && params.contam !== null){
			primary_isoform     ( extrinsic_filter.out[4], extrinsic_filter.out[5], data_input.out[10] )
                        longest_isoform     ( extrinsic_filter.out[3], data_input.out[10] )
			remove_contam	( extrinsic_filter.out[4], summary_stats.out[2], summary_stats.out[3], data_input.out[10], params.busco_lineage, data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], primary_isoform.out[0], longest_isoform.out[0])
		}
		if(params.primary_isoform == false && params.contam == null){	
			output			( data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], params.dummy, params.dummy)
		}
		if(params.primary_isoform == false && params.contam !== null){	
			remove_contam	( extrinsic_filter.out[4], summary_stats.out[2], summary_stats.out[3], data_input.out[10], params.busco_lineage, data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], params.dummy, params.dummy)
		}
		else if (params.primary_isoform == true && params.contam == null){	
            primary_isoform     ( extrinsic_filter.out[4], extrinsic_filter.out[5], data_input.out[10] )
            longest_isoform     ( extrinsic_filter.out[3], data_input.out[10] )
            output			( data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], primary_isoform.out[0], longest_isoform.out[0] )
		}
	}
	else if(params.external_protein == false){
		filter	 	( assembly_stringtie2.out[0], assembly_psiclass.out[0], data_input.out[10], assembly_stringtie2.out[1], assembly_stringtie2.out[2], assembly_psiclass.out[1], assembly_psiclass.out[2], data_input.out[6], data_input.out[0], data_input.out[9], assembly_stringtie2.out[3], assembly_psiclass.out[3])
		summary_stats		    ( filter.out[1], params.busco_lineage, filter.out[0], filter.out[3], filter.out[2], data_input.out[6], data_input.out[7], data_input.out[8] )

		if(params.primary_isoform == true && params.contam !== null ){
			primary_isoform     ( filter.out[4], filter.out[5], data_input.out[10] )
                        longest_isoform     ( filter.out[3], data_input.out[10] )
			remove_contam	( filter.out[4], summary_stats.out[2], summary_stats.out[3], data_input.out[10], params.busco_lineage, data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], primary_isoform.out[0], longest_isoform.out[0] )
		}
		if(params.primary_isoform == false && params.contam == null){
			output			( data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], params.dummy, params.dummy)
		}
		if(params.primary_isoform == false && params.contam !== null){
			remove_contam	( filter.out[4], summary_stats.out[2], summary_stats.out[3], data_input.out[10], params.busco_lineage, data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], params.dummy, params.dummy)
		}
		else if (params.primary_isoform == true && params.contam == null){
			primary_isoform     ( filter.out[4], filter.out[5], data_input.out[10] )
                        longest_isoform     ( filter.out[3], data_input.out[10] )
			output			( data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1], primary_isoform.out[0], longest_isoform.out[0])
		}
	}
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COMPLETION EMAIL AND SUMMARY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

workflow.onComplete {
    if (params.email || params.email_on_fail) {
        NfcoreTemplate.email(workflow, params, summary_params, projectDir, log )
    }
    NfcoreTemplate.summary(workflow, params, log)
    if (params.hook_url) {
        NfcoreTemplate.adaptivecard(workflow, params, summary_params, projectDir, log)
    }
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    THE END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

process gtf {
    label 'process_low'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    input:
    path(gtf)
    path(regressor)
    val(prefix)

    output: 
    path("*_filtered_primary_isoform.gtf"), emit: filtered_gtf
      
    """
    awk -F'[\t";]+' '\$3 == "transcript" {print \$10 "," \$14}' ${gtf} > gene_transcript.txt
    awk 'BEGIN {FS=","; OFS=","} NR==FNR {mapping[\$2]=\$1; next} FNR>1 {if (\$1 in mapping) print mapping[\$1], \$1, \$2}' gene_transcript.txt ${regressor} > mapped_f1.csv
    
    awk -F',' '{ key = \$1; if (\$3 > max[key]) { max[key] = \$3; value[key] = \$2; } }
    END { for (k in value) { print k "," value[k] "," max[k]; } }' mapped_f1.csv | awk -F',' '{ print \$2 }' > primary_transcript.txt

    awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /transcript_id "([^"]+)";/, m); if (m[1] in transcripts) print }' primary_transcript.txt ${gtf} > ${prefix}_filtered_primary_isoform.gtf

    """
}
process gff {
    label 'process_medium'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gtf)
    val(prefix)

    output: 
    path("*_filtered_primary_isoform.gff"), emit: filtered_gff
      
    """
    agat_convert_sp_gxf2gxf.pl --gff ${gtf} -o ${prefix}_filtered_primary_isoform.gff

    """
}
process nucleotide {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.cds"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(genome)
    path(filtered_prediction)
    val(prefix)
     
    output: 
    path("*.cds"), emit: cds
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} -t cds -o ${prefix}_filtered_primary_isoform.cds

    """
}
process protein {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.pep"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(filtered_prediction)
    path(genome)
    val(prefix)
     
    output: 
    path("*.pep"), emit: protein
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} --protein -o ${prefix}_filtered_primary_isoform.pep

    """
}
process reference {
    label 'process_low'
    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1 ' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1 ' }"

    input:
    path(reference)
    val(prefix)
     
    output: 
    path("*.gtf"), emit: gtf
      
    """
    gffread -E ${reference} -T -o ${prefix}_reference.gtf
    """   
}
process mikado {
   label 'process_low'
    publishDir "$params.outdir/metrics/mikado/filtered_primary_isoform",  mode: 'copy'
    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(reference)
    path(prediction)
    val(id)
     
    output: 
    path("$id*"), emit: prediction
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}
    """
}
process busco_isoform {
 label 'process_medium'
    publishDir "$params.outdir/metrics/busco",  mode: 'copy' 

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.6--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.6--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)
    val(prefix)

    output:
    path("filtered_primary_isoform/*"), emit: busco_filtered
    path("filtered_primary_isoform/*.txt"), emit: busco_filtered_txt
      
    """
    busco -i ${protein} -l ${odb} -o filtered_primary_isoform -m Protein -c ${task.cpus}

    """
}
process agat_isoform {
 label 'process_medium'
    publishDir "$params.outdir/metrics/agat/filtered_primary_isoform",  mode: 'copy' 

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(prediction)
    val(prefix)

    output:
    path("*.txt"), emit: filtered_stats
      
    """
    agat_sp_statistics.pl --gff ${prediction} -o ${prefix}.txt
    """
}
process diamond_isoform {
 label 'process_medium'
    publishDir "$params.outdir/08_functional_annotation/filtered_primary_isoform",  mode: 'copy', pattern: "${id}.txt" 
    
    conda "bioconda::diamond"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/diamond:2.0.6--h56fc30b_0' :
        'quay.io/biocontainers/diamond:2.0.6--h56fc30b_0' }"
   
    input:
    path(protein)
    path(database)
    val(id)
    val(qcov)
    val(scov)
 
    output:
    path("*final.txt"), emit: stats
    path("${id}.txt"), emit: annot
      
    """
   diamond blastp --more-sensitive --query-cover ${qcov} --subject-cover ${scov} -d ${database} -q ${protein} -k 1 -o ${id}.txt --threads ${task.cpus}
    count1=\$(wc -l < ${id}.txt)
    echo \$count1 > ${id}_final.txt
    """
}
process filtered_primary_output {
    label 'process_single'
    publishDir "$params.outdir/log",  mode: 'copy' 
    
    input:
    path(busco)
    path(agat)
    path(entap)
 
    output:
    path("log_filtered_primary_isoform.txt"), emit: log
      
    """
    grep -m 1 "Number of gene" ${agat} > genes.txt
    grep -m 1 "Number of transcript" ${agat} > transcripts.txt
    genes=\$(grep -o '[0-9]\\+' genes.txt)


    # Check if "Number of single exon gene" exists before creating mono.txt
    if grep -q "Number of single exon gene" ${agat}; then
       grep -m 1 "Number of single exon gene" ${agat} > mono.txt
       mono=\$(grep -o '[0-9]\\+' mono.txt)
       multi=\$((\$genes - \$mono))
       mono_multi=\$(echo "scale=2 ; \$mono / \$multi" | bc)
    else
       mono_multi="0"  
    fi

    grep "The lineage" ${busco} > odb.txt
    grep "C:" ${busco} > busco.txt

    alignments=\$(grep -o '[0-9]\\+' ${entap})
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)

    entap=\$(echo "scale=2 ; \$alignments / \$transcripts" | bc) 

    echo " " >> log_filtered_primary_isoform.txt
    echo "##### Filtered (Primary Isoform) #####" >> log_filtered_primary_isoform.txt
    echo "Total number of genes: \$genes" >> log_filtered_primary_isoform.txt
    echo "Total number of transcripts: \$transcripts" >> log_filtered_primary_isoform.txt
    echo "EnTAP alignment rate: \$entap" >> log_filtered_primary_isoform.txt
    echo "Mono-exonic/multi-exonic rate: \$mono_multi " >> log_filtered_primary_isoform.txt
    echo "BUSCO (\$odb): \$busco" >> log_filtered_primary_isoform.txt
    """
}


process gtf {
    label 'process_low'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 

    input:
    path(protein)
    path(gtf)
    val(prefix)
     
    output: 
    path("*no_contam.gtf"), emit: no_contam
      
    """

grep -h ">" ${protein} > headers.txt
sed 's/^.//' headers.txt | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /transcript_id "([^"]+)";/, m); if (m[1] in transcripts) print }' transcripts.txt ${gtf} > ${prefix}_filtered_no_contam.gtf

    """
}

process gff {
    label 'process_low'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gtf)
    val(prefix)

    output: 
    path("*no_contam.gff"), emit: filtered_gff
      
    """

agat_convert_sp_gxf2gxf.pl --gff ${gtf} --out ${prefix}_filtered_no_contam.gff

    """
}

process nucleotide {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.cds"
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(genome)
    path(filtered_prediction)
    val(prefix)
     
    output: 
    path("*no_contam.cds"), emit: cds
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} -t cds -o ${prefix}_filtered_no_contam.cds 

    """
}
process reference {
    label 'process_low'
    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1 ' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1 ' }"

    input:
    path(reference)
    val(prefix)
     
    output: 
    path("*.gtf"), emit: gtf
      
    """
    gffread -E ${reference} -T -o ${prefix}_reference.gtf
    """   
}
process mikado {
   label 'process_low'
    publishDir "$params.outdir/metrics/mikado/contam",  mode: 'copy'
    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(reference)
    path(prediction)
    val(id)
     
    output: 
    path("$id*"), emit: prediction
      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}
    """
}
process busco {
    publishDir "$params.outdir/metrics/busco",  mode: 'copy' 
    label 'process_medium'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.4--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.4--pyhdfd78af_0' }"

        
    input:
    path(protein)
    val(odb)

    output:
    path("filtered_no_contam/*"), emit: contam
    path("filtered_no_contam/*.txt"), emit: contam_txt
      
    """
    busco -i ${protein} -l ${odb}_odb10 -o filtered_no_contam -m Protein -c ${task.cpus}

    """
}
process agat {
    publishDir "$params.outdir/metrics/agat/filtered_no_contam",  mode: 'copy' 
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(protein)
    path(gff)
    val(species)

    output:
    path("*statistics.txt"), emit: contam_stats
      
    """
    agat_sp_statistics.pl --gff ${gff} -o ${species}_statistics.txt
    """
}
process no_contam {
    label 'process_single'
    publishDir "$params.outdir/log",  mode: 'copy' 
    
    input:
    path(busco)
    path(agat)
    path(entap)
    val(contam)
 
    output:
    path("log_no_contam_filtered.txt"), emit: contam_log
      
    """
    grep -m 1 "Number of gene" ${agat} > genes.txt
    grep -m 1 "Number of transcript" ${agat} > transcripts.txt
    grep "The lineage" ${busco} > odb.txt
    grep "C:" ${busco} > busco.txt
   
    alignments=\$(grep -c ">" ${entap})
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    genes=\$(grep -o '[0-9]\\+' genes.txt)
    transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)

    # Check if "Number of single exon gene" exists before creating mono.txt
    if grep -q "Number of single exon gene" ${agat}; then
       grep -m 1 "Number of single exon gene" ${agat} > mono.txt
       mono=\$(grep -o '[0-9]\\+' mono.txt)
       multi=\$((\$genes - \$mono))
       mono_multi=\$(echo "scale=2 ; \$mono / \$multi" | bc)
    else
       mono_multi="0"  
    fi

    entap=\$(echo "scale=2 ; \$alignments / \$transcripts" | bc) 

    echo " " >> filtered.txt
    echo "##### Filtered (contam removed: ${contam}) #####" >> filtered.txt
    echo "Total number of genes: \$genes" >> filtered.txt
    echo "Total number of transcripts: \$transcripts" >> filtered.txt
    echo "EnTAP alignment rate: \$entap" >> filtered.txt
    echo "Mono-exonic/multi-exonic rate: \$mono_multi " >> filtered.txt
    echo "BUSCO (\$odb): \$busco" >> filtered.txt
    mv filtered.txt log_no_contam_filtered.txt


    echo " " >> filtered.txt
    echo "##### Filtered (Contam Removed: ${contam}) #####" >> filtered.txt
    echo "Total number of genes: \$genes" >> filtered.txt
    echo "Total number of transcripts: \$transcripts" >> filtered.txt
    echo "EnTAP alignment rate: \$entap" >> filtered.txt
    echo "Mono-exonic/multi-exonic rate: \$mono_multi " >> filtered.txt
    echo "BUSCO (\$odb): \$busco" >> filtered.txt
    mv filtered.txt log_no_contam_filtered.txt
    """
}

process final_log {
    label 'process_single'
    publishDir "$params.outdir",  mode: 'copy' 

    input:
    path(contam)
    path(unfiltered)
    path(filtered)
    path(filter_primary)
    path(filter_longest)

    output:
    path("easel_summary.txt"), emit: log
    
    script:
    if (params.primary_isoform == true)
    """
    cat ${unfiltered} ${filtered} ${filter_primary} ${filter_longest} ${contam} > easel_summary.txt
    """
    else if (params.primary_isoform == false)
    """
    cat ${unfiltered} ${filtered} ${contam} > easel_summary.txt
    """
}

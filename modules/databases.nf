process entap {
    label 'process_low'

    container "plantgenomics/easel:entap"

    input:
    path(entap_config)

    output:
    path('outfiles/bin/entap_database.bin'), emit: entap_db
    path('outfiles/databases/eggnog.db'), emit: eggnog_db
    path('outfiles/bin/eggnog_proteins.dmnd'), emit: data_eggnog

    script:
    """
    EnTAP --config \\
        -t ${task.cpus}  \\
        --ini ${entap_config} \\
        --out-dir ./outfiles
    """
}
process orthodb {
    label 'process_low'

    input:
    val(order)
    val(orthodb)
    val(taxid)
    path(user_protein)

    output:
    path "*orthodb.fasta", emit: protein

    script:
    """
if readlink -f "${user_protein}" | grep -q '${projectDir}/bin/dummy.dmnd'
then
    wget https://data.orthodb.org/download/odb11v0_levels.tab.gz --no-check-certificate
    wget https://data.orthodb.org/download/odb11v0_level2species.tab.gz --no-check-certificate
    wget https://data.orthodb.org/download/odb11v0_species.tab.gz --no-check-certificate
    gzip -d odb11v0_species.tab.gz

    if echo "${order}" | grep -q 'null'
    then
        echo "Order not provided, refer to ${projectDir}/bin/orthodb.txt"
        exit 1

    elif awk -F'\\t' '\$1 == "${order}"' ${projectDir}/bin/orthodb.txt 
    then
        protein=\$(awk -F'\\t' '\$1 == "${order}" { print \$2 }' ${projectDir}/bin/orthodb.txt)
        wget https://treegenesdb.org/FTP/OrthoDB/v11/"\$protein".fasta.gz --no-check-certificate
        gzip -d "\$protein".fasta.gz

        GROUP="${order}"
        level=`zcat odb11v0_levels.tab.gz | awk -v grp=\$GROUP '\$2 == grp'`
        taxid=`echo \$level | cut -d ' ' -f 1`
        species=`zcat odb11v0_level2species.tab.gz \\
        | awk -v taxid=\$taxid '\$4 ~ "{"\$taxid"}" || 
                       \$4 ~ "{"taxid"," || 
                       \$4 ~ ","taxid"," || 
                       \$4 ~ ","taxid"}" {print \$2}'`
        echo \$species | wc -w
        echo \$level
        regex=`echo \$species | tr ' ' '|'`
        grep --no-group-separator -A 1 -P -w "\$regex" "\$protein".fasta > "\$GROUP"_orthodb.fasta
    else
        echo "Order not found! refer to ${projectDir}/bin/orthodb.txt"
    fi 

    if [ -z "${taxid}" ]; then
        echo "--taxid not provided, keeping species in orthodb database"
    elif awk -F'\\t' '\$1 == "${taxid}"' odb11v0_species.tab
    then
        id=\$(awk -F'\\t' '\$1 == "${taxid}" { print \$2 }' odb11v0_species.tab)
        tax_species=\$(awk -F'\\t' '\$1 == "${taxid}" { print \$3 }' odb11v0_species.tab)
        echo \$id
        echo \$tax_species
        grep \$id "\$GROUP"_orthodb.fasta > header.txt
        if [ ! -s "header.txt" ]; then 
            echo "taxid does not exist in database"
            exit 1
        else
            sed -i 's/^.//' header.txt
            awk 'FNR==NR { headers[\$0]; next } /^>/ { header=\$0; sub(/^>/, "", header); if (!(header in headers)) print; print_seq = !(header in headers); next } print_seq { print }' header.txt "\$GROUP"_orthodb.fasta > no_${taxid}_"\$GROUP"_orthodb.fasta
            rm "\$GROUP"_orthodb.fasta
        fi
    else
        echo "taxid does not exist!"
        exit 1
    fi 
else 
    cp ${user_protein} orthodb.fasta
fi
    """
}

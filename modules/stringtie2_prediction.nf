process ESTalign_stringtie2 {
    publishDir "$params.outdir/03_alignments/est",  mode: 'copy'
    label 'process_medium'

    conda "bioconda::gmap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2' :
        'quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2' }"


    input:
    path(cds)
    path(gmapIndex)
    path(genome)
     
    output:
    path "eggnog.stringtie2.psl", emit: stringtie2_align
    
    script:
    """
    genome_size=\$(awk '!/^>/ { gsub("[^A-Za-z]", ""); count += length } END { print count }' ${genome})
    if (( \$genome_size < 4294967296 )); then
    	gmap -D ${gmapIndex} -d gmap ${cds} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > eggnog.stringtie2.psl 
    else
	gmapl -D ${gmapIndex} -d gmap ${cds} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > eggnog.stringtie2.psl
    fi
    
    """
}
process ESThints_stringtie2 {
    publishDir "$params.outdir/05_hints/stringtie2",  mode: 'copy'
    label 'process_medium'

    container "plantgenomics/easel:perl"

    input:
    path(gmap)
     
    output:
    path "eggnog.estHints.gff", emit: stringtie2_est
    
    script:
    """
    cat ${gmap} | sort -n -k 16,16 | sort -s -k 14,14 | perl -ne '@f=split; print if (\$f[0]>=100)' | ${projectDir}/bin/blat2hints.pl --source=E --nomult --ep_cutoff=20 --in=/dev/stdin --out=eggnog.estHints.gff
    """
}
process miniprot_stringtie2 {
    label 'process_high'
    publishDir "$params.outdir/03_alignments/protein",  mode: 'copy'
    
    conda "bioconda::miniprot"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/miniprot:0.11--he4a0461_2' :
        'quay.io/biocontainers/miniprot:0.11--he4a0461_2' }" 

    input:
    path(genome)
    path(psiclasstransdecoderPEP)

    output:
    path "eggnog.stringtie2.gtf", emit: stringtie2_miniprot

    script:
    """
    miniprot -Iut${task.cpus} ${genome} ${psiclasstransdecoderPEP} --gtf > eggnog.stringtie2.gtf
    """
}
process proteinHints_stringtie2 {
    publishDir "$params.outdir/05_hints/stringtie2",  mode: 'copy' 
    label 'process_low'
    container 'plantgenomics/easel:perl'

    input:
    path(miniprot)
    path(genome)

    output:
    path "eggnog.protHints.gff", emit: stringtie2_protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=eggnog.protHints.gff --prg=miniprot --genome_file=${genome}
    """
}
process gffread_gtf {
    label 'process_low'

    conda "bioconda::gffread=0.12.7"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    path(stringtie2Gmap)
     
    output:
    path "*.gtf", emit: gtf
    
    script:
    """
    gffread ${stringtie2Gmap} -T -o stringtie2.gtf
    """
}
process trainingSet_stringtie2 {
    label 'process_high_cpu'

    container "plantgenomics/easel:augustus"
    
    input:
    path(gtf)
    path(gff)
    path(genome)
    val(species)
    val(test)
    val(train)
    val(kfold)
    val(rounds)
     
    output:
    path "*.gb", emit: gb
    
    script:
  if (params.optimize == true )
  """
  if [ ! -d ~/.easel ]; then
  mkdir ~/.easel
  cp -r /usr/share/augustus ~/.easel
  fi
  chmod -R 775 ~/.easel
  export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
  /usr/share/augustus/scripts/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
  flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
  echo "\$flanking_DNA" >> calculate_flank.out
  /usr/share/augustus/scripts/gff2gbSmallDNA.pl ${gff} ${genome} \$flanking_DNA stringtie2Raw.gb
  if [[ -d ~/.easel/augustus/config/species/"$species"_stringtie2 ]]; then rm -rf ~/.easel/augustus/config/species/"$species"_stringtie2; fi
  ~/.easel/augustus/scripts/new_species.pl --species="$species"_stringtie2
  /usr/bin/etraining --species="$species"_stringtie2 stringtie2Raw.gb 2> failed_genes.txt
  awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
  ~/.easel/augustus/scripts/filterGenes.pl badTrainingGenes.lst stringtie2Raw.gb > stringtie2GoodGenes.gb
  grep -c "LOCUS" *.gb | tee calculate_flank.out
  ~/.easel/augustus/scripts/randomSplit.pl stringtie2GoodGenes.gb ${test}
  ~/.easel/augustus/scripts/randomSplit.pl stringtie2GoodGenes.gb.train ${train}
  rm stringtie2GoodGenes.gb.train
  rm stringtie2GoodGenes.gb.train.train
  mv stringtie2GoodGenes.gb.train.test train.gb
  mv stringtie2GoodGenes.gb.test test.gb
  grep -c "LOCUS" *.gb >> calculate_flank.out
  /usr/bin/etraining --species="$species"_stringtie2 train.gb
  /usr/bin/augustus --species="$species"_stringtie2 test.gb | tee stringtie2_untrained.out

  if [ "${kfold}" -eq 0 ]
  then 
  	~/.easel/augustus/scripts/optimize_augustus.pl --species="$species"_stringtie2 train.gb --cpus=${task.cpus} --kfold=${task.cpus} --rounds=${rounds}
  else
        ~/.easel/augustus/scripts/optimize_augustus.pl --species="$species"_stringtie2 train.gb --cpus=${task.cpus} --kfold=${kfold} --rounds=${rounds}	
  fi
  /usr/bin/etraining --species="$species"_stringtie2 train.gb
  /usr/bin/augustus --species="$species"_stringtie2 test.gb | tee stringtie2_optimized.out
   """
   else if (params.optimize == false )
   """
  if [ ! -d ~/.easel ]; then
  mkdir ~/.easel
  cp -r /usr/share/augustus ~/.easel
  fi 
  chmod -R 775 ~/.easel
  export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
  /usr/share/augustus/scripts/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
  flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
  echo "\$flanking_DNA" >> calculate_flank.out
  /usr/share/augustus/scripts/gff2gbSmallDNA.pl ${gff} ${genome} \$flanking_DNA stringtie2Raw.gb
  if [[ -d ~/.easel/augustus/config/species/"$species"_stringtie2 ]]; then rm -rf ~/.easel/augustus/config/species/"$species"_stringtie2; fi
  ~/.easel/augustus/scripts/new_species.pl --species="$species"_stringtie2 
  /usr/bin/etraining --species="$species"_stringtie2 stringtie2Raw.gb 2> failed_genes.txt
  awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst 
  ~/.easel/augustus/scripts/filterGenes.pl badTrainingGenes.lst stringtie2Raw.gb > stringtie2GoodGenes.gb
  grep -c "LOCUS" *.gb | tee calculate_flank.out
  ~/.easel/augustus/scripts/randomSplit.pl stringtie2GoodGenes.gb ${test}
  ~/.easel/augustus/scripts/randomSplit.pl stringtie2GoodGenes.gb.train ${train}
  rm stringtie2GoodGenes.gb.train
  rm stringtie2GoodGenes.gb.train.train
  mv stringtie2GoodGenes.gb.train.test train.gb
  mv stringtie2GoodGenes.gb.test test.gb
  grep -c "LOCUS" *.gb >> calculate_flank.out
  /usr/bin/etraining --species="$species"_stringtie2 train.gb
  /usr/bin/augustus --species="$species"_stringtie2 test.gb | tee stringtie2_untrained.out
  """
}

process augustusEST_stringtie2 {
    label 'process_low'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(gb)
    path(ESThints)
    path(configEST)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.ESTHints.gff3", emit: stringtie2_estHints
     
    script: 
    """
    export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
    /usr/bin/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.ESTHints.gff3
    """
}

process augustusProtein_stringtie2 {
    label 'process_low'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(gb)
    path(proteinHints)
    path(configProtein)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.protHints.gff3", emit: stringtie2_proteinHints
    
    script:  
    """
    export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
    /usr/bin/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.protHints.gff3

    """
}
process combineEST_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(estHints)
     
    output:
    path("est_stringtie2.gff"), emit: estGFF
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${estHints} est_stringtie2.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o est_stringtie2.gff
fi

    """
}

process combineProtein_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(proteinHints)
     
    output:
    path("protein_stringtie2.gff"), emit: stringtie_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${proteinHints} protein_stringtie2.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o protein_stringtie2.gff
fi
    """
}

process combineAll_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(augustus_protein)
    path(augustus_est)
     
    output:
    path("stringtie2_unfiltered.gff"), emit: stringtie2_combined
      
    """

agat_sp_merge_annotations.pl -f ${augustus_protein} -f ${augustus_est} -o stringtie2_unfiltered.gff

    """
}
process fix_exons_stringtie2 {
    label 'process_medium_memory'

    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    path(gff)
    
     
    output:
    path("stringtie2_fix_exons.gff"), emit: fixed
    
    """
    gffread ${gff} -Z -o stringtie2_fix_exons.gff
    """
}

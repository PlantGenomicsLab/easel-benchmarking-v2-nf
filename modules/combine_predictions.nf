process psiclass_utr_trim {
    label 'process_medium_memory'

    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"


    input:
    path(gff)
     
    output:
    path("psiclass_trim_utr.gff"), emit: psi_utr
    
    """
    sed 's/geneID/gene_id/g' ${gff} | awk '\$3 !~ /^(exon|five_prime_UTR|three_prime_UTR)\$/' | gffread --force-exons --keep-genes > psiclass_trim_utr.gff

    """
}
process stringtie2_utr_trim {
    label 'process_medium_memory'

    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"


    input:
    path(gff)

    output:
    path("stringtie2_trim_utr.gff"), emit: str_utr
    
    """
    sed 's/geneID/gene_id/g' ${gff} | awk '\$3 !~ /^(exon|five_prime_UTR|three_prime_UTR)\$/' | gffread -Z --force-exons --keep-genes > stringtie2_trim_utr.gff

    """
}
process psiclass_filter {
    label 'process_medium_memory'
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(gff)
    path(genome)
     
    output:
    path("psiclass.gff"), emit: psi_start
    
    """
    agat_sp_filter_incomplete_gene_coding_models.pl --gff ${gff} --fasta ${genome} -o psiclass.gff
    sed -i 's/\tmRNA\t/\ttranscript\t/g' psiclass.gff

    """
}

process stringtie2_filter {
    label 'process_medium_memory'
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(gff)
    path(genome)
     
    output:
    path("stringtie2.gff"), emit: str_start
    
    """
    agat_sp_filter_incomplete_gene_coding_models.pl --gff ${gff} --fasta ${genome} -o stringtie2.gff
    sed -i 's/\tmRNA\t/\ttranscript\t/g' stringtie2.gff 

    """
}
process merge_gff {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(stringtie2)
    path(psiclass)
    path(stringtie2_trans)
    path(psiclass_trans)

     
    output:
    path("merged_unfiltered.gff"), emit: merged_unfiltered
    
    """
    agat_sp_merge_annotations.pl -f ${stringtie2} -f ${psiclass} -f ${stringtie2_trans} -f {psiclass_trans} -o merged_unfiltered.gff
    
    sed -i 's/\ttranscript\t/\tmRNA\t/g' merged_unfiltered.gff

    """
}
process gene_overlap {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(gff)


    output:
    path("overlap_unfiltered.gff"), emit: overlap_unfiltered

    """
    agat_sp_fix_overlaping_genes.pl --gff ${gff} -o overlap_unfiltered.gff

    sed -i 's/\tmRNA\t/\ttranscript\t/g' overlap_unfiltered.gff

    """
}

process format_gff {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    path(genome)
    val(id)
     
    output:
    path("*_unfiltered.gff"), emit: unfiltered_fixed
    
    """
    agat_sp_add_start_and_stop.pl --gff ${gff} --fasta ${genome} --out start_stop.gff
    sed 's/;Name.*//g' start_stop.gff | sed 's/;geneID.*//g' | grep -v '^#' > start_unfiltered.gff 

    """
}
process mergeGFF_orthodb {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(stringtie2)
    path(psiclass)
    path(s_ortho)
    path(p_ortho)
    path(stringtie2_trans)
    path(psiclass_trans)

     
    output:
    path("merged_unfiltered.gff"), emit: unfiltered_db
    
    """
    agat_sp_merge_annotations.pl -f ${stringtie2} -f ${psiclass} -f ${s_ortho} -f ${p_ortho} -f ${stringtie2_trans} -f ${psiclass_trans} -o merged_unfiltered.gff
    sed -i 's/\ttranscript\t/\tmRNA\t/g' merged_unfiltered.gff
    """
}
process list_orthodb {
    label 'process_single'
    
    input:
    path(s_est)
    path(s_prot)
    path(p_est)
    path(p_prot)
    path(p_orthodb)
    path(s_orthodb)
    path(s_trans)
    path(p_trans)


    output:
    path("list.txt"), emit: gff
      
    """
    readlink -f ${s_est} > list.txt
    readlink -f ${s_prot} >> list.txt
    readlink -f ${p_est} >> list.txt
    readlink -f ${p_prot} >> list.txt
    readlink -f ${p_orthodb} >> list.txt
    readlink -f ${s_orthodb} >> list.txt
    readlink -f ${s_trans} >> list.txt
    readlink -f ${p_trans} >> list.txt

    """
}
process list {
    label 'process_single'
    
    input:
    path(s_est)
    path(s_prot)
    path(p_est)
    path(p_prot)
    path(s_trans)
    path(p_trans)


    output:
    path("list.txt"), emit: gff

    """
    readlink -f ${s_est} > list.txt
    readlink -f ${s_prot} >> list.txt
    readlink -f ${p_est} >> list.txt
    readlink -f ${p_prot} >> list.txt
    readlink -f ${s_trans} >> list.txt
    readlink -f ${p_trans} >> list.txt
    """
}

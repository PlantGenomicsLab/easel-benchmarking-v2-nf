process easel_output {
    label 'process_single'
    publishDir "$params.outdir",  mode: 'copy' 

    input:
    path(unfiltered)
    path(filtered)
    path(filter_primary)
    path(filter_longest)

    output:
    path("easel_summary.txt"), emit: log
    
    script:
    if (params.primary_isoform == true)
    """
    cat ${unfiltered} ${filtered} ${filter_primary} ${filter_longest} > easel_summary.txt

    """
    else if (params.primary_isoform == false)
    """
    cat ${unfiltered} ${filtered} > easel_summary.txt

    """
}

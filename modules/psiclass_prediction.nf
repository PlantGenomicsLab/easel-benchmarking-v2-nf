process ESTalign_psiclass {
    publishDir "$params.outdir/03_alignments/est",  mode: 'copy'

    label 'process_high'
    
    conda "bioconda::gmap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2' :
        'quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2' }"


    input:
    path(cds)
    path(gmapIndex)
    path(genome)
     
    output:
    path "eggnog.psiclass.psl", emit: psiclass_align
    
    script:
    """
    genome_size=\$(awk '!/^>/ { gsub("[^A-Za-z]", ""); count += length } END { print count }' ${genome})
    if (( \$genome_size < 4294967296 )); then	
    	gmap -D ${gmapIndex} -d gmap ${cds} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > eggnog.psiclass.psl 
    else
	gmapl -D ${gmapIndex} -d gmap ${cds} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > eggnog.psiclass.psl
    fi
    """
}
process ESThints_psiclass {
    publishDir "$params.outdir/05_hints/psiclass",  mode: 'copy'
    label 'process_medium'
    
    container "plantgenomics/easel:perl"


    input:
    path(gmap)
     
    output:
    path "eggnog.estHints.gff", emit: psiclass_est
    
    script:
    """

    cat ${gmap} | sort -n -k 16,16 | sort -s -k 14,14 | perl -ne '@f=split; print if (\$f[0]>=100)' | ${projectDir}/bin/blat2hints.pl --source=E --nomult --ep_cutoff=20 --in=/dev/stdin --out=eggnog.estHints.gff

    """
}
process miniprot_psiclass {
    label 'process_high'
    publishDir "$params.outdir/03_alignments/protein",  mode: 'copy'

    conda "bioconda::miniprot"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/miniprot:0.11--he4a0461_2' :
        'quay.io/biocontainers/miniprot:0.11--he4a0461_2' }" 

    input:
    path(genome)
    path(psiclasstransdecoderPEP)


    output:
    path "eggnog.psiclass.gtf", emit: psiclass_miniprot

    script:
    """
    miniprot -Iut${task.cpus} ${genome} ${psiclasstransdecoderPEP} --gtf > eggnog.psiclass.gtf
    """
}
process proteinHints_psiclass {
    publishDir "$params.outdir/05_hints/psiclass",  mode: 'copy' 
    label 'process_low'
    container 'plantgenomics/easel:perl'
    

    input:
    path(miniprot)
    path(genome)

    output:
    path "eggnog.protHints.gff", emit: psiclass_protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=eggnog.protHints.gff --prg=miniprot --genome_file=${genome}
    """
}
process gffread_gtf {
    label 'process_low'

    conda "bioconda::gffread=0.12.7"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    path(psiclassGmap)
     
    output:
    path "*.gtf", emit: gtf
      
    """
    gffread ${psiclassGmap} -T -o psiclass.gtf
    """
}
process trainingSet_psiclass {
    label 'process_high_cpu'

    container "plantgenomics/easel:augustus"
    
    input:
    path(gtf)
    path(gff)
    path(genome)
    val(species)
    val(test)
    val(train)
    val(kfold)
    val(rounds)
     
    output:
    path "*.gb", emit: gb
  
  script: 
  
  if (params.optimize == true )    
  """
  if [ ! -d ~/.easel ]; then
  mkdir ~/.easel
  cp -r /usr/share/augustus ~/.easel
  fi
  chmod -R 775 ~/.easel
  export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
  /usr/share/augustus/scripts/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
  flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
  echo "\$flanking_DNA" >> calculate_flank.out
  /usr/share/augustus/scripts/gff2gbSmallDNA.pl ${gff} ${genome} \$flanking_DNA psiclassRaw.gb
  if [[ -d ~/.easel/augustus/config/species/"$species"_psiclass ]]; then rm -rf ~/.easel/augustus/config/species/"$species"_psiclass; fi
  ~/.easel/augustus/scripts/new_species.pl --species="$species"_psiclass
  /usr/bin/etraining --species="$species"_psiclass psiclassRaw.gb 2> failed_genes.txt
  awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
  ~/.easel/augustus/scripts/filterGenes.pl badTrainingGenes.lst psiclassRaw.gb > psiclassGoodGenes.gb
  grep -c "LOCUS" *.gb | tee calculate_flank.out
  ~/.easel/augustus/scripts/randomSplit.pl psiclassGoodGenes.gb ${test}
  ~/.easel/augustus/scripts/randomSplit.pl psiclassGoodGenes.gb.train ${train}
  rm psiclassGoodGenes.gb.train
  rm psiclassGoodGenes.gb.train.train
  mv psiclassGoodGenes.gb.train.test train.gb
  mv psiclassGoodGenes.gb.test test.gb
  grep -c "LOCUS" *.gb >> calculate_flank.out
  /usr/bin/etraining --species="$species"_psiclass train.gb
  /usr/bin/augustus --species="$species"_psiclass test.gb | tee psiclass_untrained.out
  if [ "${kfold}" -eq 0 ]
  then
  	~/.easel/augustus/scripts/optimize_augustus.pl --species="$species"_psiclass train.gb --cpus=${task.cpus} --kfold=${task.cpus} --rounds=${rounds}
  else
  	~/.easel/augustus/scripts/optimize_augustus.pl --species="$species"_psiclass train.gb --cpus=${task.cpus} --kfold=${kfold} --rounds=${rounds}
  fi
  /usr/bin/etraining --species="$species"_psiclass train.gb
  /usr/bin/augustus --species="$species"_psiclass test.gb | tee psiclass_optimized.out
  """
  else if (params.optimize == false )
  """
  if [ ! -d ~/.easel ]; then
  mkdir ~/.easel
  cp -r /usr/share/augustus ~/.easel
  fi
  chmod -R 775 ~/.easel
  export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
  /usr/share/augustus/scripts/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
  flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
  echo "\$flanking_DNA" >> calculate_flank.out
  /usr/share/augustus/scripts/gff2gbSmallDNA.pl ${gff} ${genome} \$flanking_DNA psiclassRaw.gb
  if [[ -d ~/.easel/augustus/config/species/"$species"_psiclass ]]; then rm -rf ~/.easel/augustus/config/species/"$species"_psiclass; fi
  ~/.easel/augustus/scripts/new_species.pl --species="$species"_psiclass
  /usr/bin/etraining --species="$species"_psiclass psiclassRaw.gb 2> failed_genes.txt
  awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
  ~/.easel/augustus/scripts/filterGenes.pl badTrainingGenes.lst psiclassRaw.gb > psiclassGoodGenes.gb
  grep -c "LOCUS" *.gb | tee calculate_flank.out
  ~/.easel/augustus/scripts/randomSplit.pl psiclassGoodGenes.gb ${test}
  ~/.easel/augustus/scripts/randomSplit.pl psiclassGoodGenes.gb.train ${train}
  rm psiclassGoodGenes.gb.train
  rm psiclassGoodGenes.gb.train.train
  mv psiclassGoodGenes.gb.train.test train.gb
  mv psiclassGoodGenes.gb.test test.gb
  grep -c "LOCUS" *.gb >> calculate_flank.out
  /usr/bin/etraining --species="$species"_psiclass train.gb
  /usr/bin/augustus --species="$species"_psiclass test.gb | tee psiclass_untrained.out
  """
}

process augustusEST_psiclass {
    label 'process_low'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(gb)
    path(ESThints)
    path(configEST)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.ESTHints.gff3", emit: psiclass_estHints
      
    """
    export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
    /usr/bin/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.ESTHints.gff3
    """
}

process augustusProtein_psiclass {
    label 'process_low'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(gb)
    path(proteinHints)
    path(configProtein)
    tuple val(id), path(genome_chunk)
    val(species)
    
    output:
    path "*psiclass.protHints.gff3", emit: psiclass_proteinHints
      
    """
    export AUGUSTUS_CONFIG_PATH=~/.easel/augustus/config
    /usr/bin/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.protHints.gff3

    """
}

process combineEST_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(estHints)
     
    output:
    path("est_psiclass.gff"), emit: estGFF
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${estHints} est_psiclass.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o est_psiclass.gff
fi

    """
}

process combineProtein_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(proteinHints)
     
    output:
    path("protein_psiclass.gff"), emit: psiclass_proteinHints
      
    """

mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${proteinHints} protein_psiclass.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o protein_psiclass.gff
fi
    """
}

process combineAll_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    input:
    path(augustus_protein)
    path(augustus_est)
     
    output:
    path("psiclass_unfiltered.gff"), emit: psiclass_combined
      
    """

agat_sp_merge_annotations.pl -f ${augustus_protein} -f ${augustus_est} -o psiclass_unfiltered.gff

    """
}
process fix_exons_psiclass {
    label 'process_medium_memory'

    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    path(gff)
    
     
    output:
    path("psiclass_fix_exons.gff"), emit: fixed
    
    """
    gffread ${gff} -Z -o psiclass_fix_exons.gff
    """
}

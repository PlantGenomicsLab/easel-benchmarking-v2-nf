def helpMessage() {
	log.info"""
	========================================================================================
                 EASEL - Efficient, Accurate Scalable Eukaryotic modeLs
	========================================================================================
 	
	Usage:
	nextflow run -hub gitlab PlantGenomicsLab/easel --genome /path/to/genome.fa --busco_lineage <busco_lineage> --order <orthodb_order> --taxon <taxon> --sra /path/to/sralist.txt --training_set <plant/invertebrate> [...] 
	or
	nextflow run -hub gitlab PlantGenomicsLab/easel -profile <singularity/docker> -params-file params.yaml
	
	Required arguments:
		--genome				 Path to masked genome (*.fa/*.fasta)
		--sra					 Path to SRA accession list
		  or
		--user_reads		                 Path to user RNA reads (*_{1,2}.fastq)
		  or
		--bam					 Path to RNA alignemtns (*.bam)
		--busco_lineage			         Relevant OrthoDB protein database as source of reference protein sequence (example: embryophyta)
		--taxon					 Relevant taxon for functional annotation
       	        --order					 Taxonomic rank such as order/clade for OrthoDB protein filtering
                --training_set				 Random forest clade specific training set for feature filtering (plant or invertebrate)

	Recommended arguments:
		--outdir				 Path to the output directory (default: easel_taxon)
		--prefix				 AUGUSTUS training identifier (default: taxon)
                --reference_db		                 DIAMOND datbase for EnTAP fnctional annotation (default: complete RefSeq)
                --external_protein		         Add OrthoDB proteins to Augustus analysis (default: true)
		--max_memory          	                 Maximum memory allocated
	        --max_cpus         	                 Maximum cpus allocated
    	        --max_time                               Maximum time allocated
    		--longest_isoform			 Output alternative transcripts only (default: true)
	    	--contam			         Remove contaminants from functional annotation (default: null)

	Optional arguments:
		--bins					 Number of bins genome will be split into (default: 50)
		--parts				         Number of bins true start sites will be split into (default: 5)
                --percent_masked			 Filtering repeat masked cut-off (default: 10)
		--rate					 Filtering mapping rate cut-off (default: 85)
		--total_reads		                 Filtering read length cut-off (default: 15000000)
		--mean_length		                 Filtering mean length of reads cut-off (default: 70)
		--cluster_id		                 VSEARCH identify threshold (default: 0.80)
		--gap_tolerance			         StringTie2: gap tolerance (default: 0)
		--hisat2_min_intronlen                   HISAT2: min inton length (default: 20)
		--hisat2_max_intronlen                   HISAT2: max inton length (default: 500000)
		--test					 Number of genes to test AUGUSTUS (default: 250)
		--train					 Number of genes to train AUGUSTUS (default: 1000)
		--kfold					 K-fold cross validation of AUGUSTUS optimization  (default: 8)
		--rounds				 Number of rounds for AUGUSTUS optimization (default: 5)
		--optimize				 Optimize AUGUSTUS (default: true)
		--tcoverage				 EnTAP t-coverage cutoff (default: 70)
		--qcoverage				 EnTAP q-coverage cutoff (default: 70)
		--window				 RNA folding window size, upstream_downstream (default: 400_200)
		--regressor				 Threshold for filtering random forest F1 prediction (default: 80)
		--taxid				 	 Taxonomic ID (ex: 3702) to remove species from OrthoDB database (default: null)
                --user_protein				 User OrthoDB input proteins (default: null)

	Resuming nextflow:
		-resume					Resume script from last step 
}
	
	Documentation:
	https://gitlab.com/PlantGenomicsLab/easel

 	Contact:
	Cynthia Webster <cynthia.webster@uconn.edu>
	Jill Wegrzyn <jill.wegrzyn@uconn.edu>
	========================================================================================

    """.stripIndent()
}

params.help = false
if (params.help){
    helpMessage()
    exit 0
}

nextflow.enable.dsl=2

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE & PRINT PARAMETER SUMMARY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

WorkflowMain.initialise(workflow, params, log)


/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    RUN ALL WORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { main_workflow as MAIN } from './workflows/main_workflow.nf'

workflow {

    MAIN()

    }

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    THE END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
